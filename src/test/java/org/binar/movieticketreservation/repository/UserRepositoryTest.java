package org.binar.movieticketreservation.repository;

import org.binar.movieticketreservation.entity.Users;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@Sql(scripts = "/sql/delete-user.sql")
public class UserRepositoryTest {
    @Autowired
    UserRepository userRepository;

    @Test
    public void createUser(){
        Users user = new Users();
        user.setUsername("test_aja");
        user.setEmail("test_aja@gmail.com");

        Assertions.assertNull(user.getId());
        userRepository.save(user);
        Assertions.assertNotNull(user.getId());
    }

    @Test
    @Sql(scripts = "/sql/insert-user.sql")
    public void testSoftDelete(){
        Users userDeleted = userRepository.findById("test001").get();
        userRepository.delete(userDeleted);

        // deleted data which mean the data with statusRecord = 'INACTIVE' is does not count
        Long userCount = userRepository.count();
        Assertions.assertEquals(2, userCount);
    }
}
