package org.binar.movieticketreservation.service;

import org.binar.movieticketreservation.entity.Users;
import org.binar.movieticketreservation.exception.IdNotFoundException;
import org.binar.movieticketreservation.exception.UserNotFoundException;
import org.binar.movieticketreservation.exception.UsernameTakenException;
import org.binar.movieticketreservation.exception.WrongPasswordException;
import org.binar.movieticketreservation.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class UserServiceTest {
    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void A_SuccessAddUserTest() throws UsernameTakenException {
        Users newUser = new Users();
        newUser.setUsername("test");
        newUser.setEmail("test@gmail.com");
        newUser.setPassword("testPass");

        Assertions.assertNull(newUser.getId());
        userService.addUser(newUser);
        Assertions.assertNotNull(newUser.getId());
    }

    @Test
    public void B_FailAddUserTest() {
        Exception exception = Assertions.assertThrows(Exception.class, () ->{
            Users newUser = new Users();
            // will give an UsernameTakenException
            newUser.setUsername("test");
            userService.addUser(newUser);
        });
        String expectedMessage = "username not available";
        String actualMessage = exception.getMessage();
        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void D_GetAllUsers(){
        List<Users> users = userService.getUsers();
        System.out.println(users);
    }

    @Test
    public void E_DeleteUser() throws UsernameTakenException, IdNotFoundException {
        Users newUser = new Users();

        newUser.setUsername("test_username_deleted");
        newUser.setEmail("test_deleted@gmail.com");
        newUser.setPassword("testPass_deleted");

        userService.addUser(newUser);
        userService.deleteUserById(newUser.getId());

        Long userCount = userRepository.count();
        Assertions.assertEquals(1, userCount);
    }
}
