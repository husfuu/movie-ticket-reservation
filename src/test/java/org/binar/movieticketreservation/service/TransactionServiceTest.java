package org.binar.movieticketreservation.service;

import org.binar.movieticketreservation.dto.TransactionHistoryResponse;
import org.binar.movieticketreservation.dto.TransactionRequest;
import org.binar.movieticketreservation.entity.TransactionStatus;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class TransactionServiceTest {
    @Autowired
    private TransactionHistoryService transactionHistoryService;

    @Test
    public void A_TestCreateTransaction(){
        TransactionRequest transactionRequest = new TransactionRequest();
        transactionRequest.setStatus(TransactionStatus.valueOf("PENDING"));
        transactionRequest.setUserId("U01");
        transactionRequest.setScheduleId("S01");
        transactionRequest.setFilmId("F01");
        transactionRequest.setStudioId("ST_01");

        transactionHistoryService.AddTransaction(transactionRequest);
    }

    @Test
    public void B_TestUpdateStatusTransaction(){
        String userInput = "SUCCESS";
        String id = "89fb9ba4-193f-4bed-8cc9-3b8312c7caab";

        transactionHistoryService.UpdateStatusTransaction(id, userInput);
    }

    @Test
    public void C_TestGetAllTransactions(){
        List<TransactionHistoryResponse> transactions = transactionHistoryService.getTransactions();
        System.out.println(transactions);
    }
}
