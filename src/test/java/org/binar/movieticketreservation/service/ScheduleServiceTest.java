package org.binar.movieticketreservation.service;

import org.binar.movieticketreservation.dto.FilmScheduleRequest;
import org.binar.movieticketreservation.dto.FilmScheduleUpdateRequest;
import org.binar.movieticketreservation.dto.ScheduleResponse;
import org.binar.movieticketreservation.entity.Schedule;
import org.binar.movieticketreservation.exception.IdNotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootTest
public class ScheduleServiceTest {
    @Autowired
    private ScheduleService scheduleService;

    @Test
    public void A_GetScheduleInformation(){
        List<ScheduleResponse> scheduleResponses = scheduleService.getFilmSchedules();
        System.out.println(scheduleResponses);
    }

    @Test
    public void C_AddFilmSchedule(){
        FilmScheduleRequest filmScheduleRequest = new FilmScheduleRequest();
        filmScheduleRequest.setFilmName("The Hobbit");
        filmScheduleRequest.setOnShow(true);
        filmScheduleRequest.setShow_time(LocalDateTime.now());
        filmScheduleRequest.setStart_time(LocalDateTime.now());
        filmScheduleRequest.setEnd_time(LocalDateTime.now());
        scheduleService.AddFilmSchedules(filmScheduleRequest);
    }

    @Test
    public void D_UpdateFilmSchedule() throws IdNotFoundException {
        FilmScheduleUpdateRequest filmScheduleUpdateRequest = new FilmScheduleUpdateRequest();
        String scheduleId = "S01";
        filmScheduleUpdateRequest.setFilmName("The Batman");
        filmScheduleUpdateRequest.setStudioName("Studio C");

        scheduleService.UpdateFilmSchedule(scheduleId, filmScheduleUpdateRequest);
    }
}
