package org.binar.movieticketreservation.service;

import org.binar.movieticketreservation.dto.UserUpdateRequest;
import org.binar.movieticketreservation.entity.Users;
import org.binar.movieticketreservation.exception.IdNotFoundException;
import org.binar.movieticketreservation.exception.UsernameTakenException;
import org.binar.movieticketreservation.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service @Transactional
public class UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public List<Users> getUsers(){
        return userRepository.findAll();
    }

    public void addUser(Users user) throws UsernameTakenException{
        Optional<Users> userOptional = userRepository.findUserByUsername(user.getUsername());

        if (userOptional.isPresent()){
            throw new UsernameTakenException("username not available");
        }
        userRepository.save(user);
    }

    public void updateUserById(String id, UserUpdateRequest requestUpdateUser) throws IdNotFoundException {
        Optional<Users> userOptional = userRepository.findById(id);

        if (userOptional.isPresent()){
            throw new IdNotFoundException("user with " + id + " is not found!");
        }

        userOptional.get().setEmail(requestUpdateUser.getEmail());
        userOptional.get().setUsername(requestUpdateUser.getUsername());
        userOptional.get().setPassword(requestUpdateUser.getPassword());

        userRepository.save(userOptional.get());
    }

    public void deleteUserById(String id) throws IdNotFoundException{
        Optional<Users> userOptional = userRepository.findById(id);

        if (!userOptional.isPresent()){
            throw new IdNotFoundException("user with id " + id + " is not found");
        }

        userRepository.delete(userOptional.get());
    }
}
