package org.binar.movieticketreservation.service;

import org.binar.movieticketreservation.dto.TransactionHistoryResponse;
import org.binar.movieticketreservation.dto.TransactionRequest;
import org.binar.movieticketreservation.entity.*;
import org.binar.movieticketreservation.repository.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TransactionHistoryService {
    private final TransactionHistoryRepository transactionHistoryRepository;
    private final FilmRepository filmRepository;
    private final ScheduleRepository scheduleRepository;
    private final StudioRepository studioRepository;
    private final UserRepository userRepository;

    public TransactionHistoryService(TransactionHistoryRepository transactionHistoryRepository, FilmRepository filmRepository, ScheduleRepository scheduleRepository, StudioRepository studioRepository, UserRepository userRepository) {
        this.transactionHistoryRepository = transactionHistoryRepository;
        this.filmRepository = filmRepository;
        this.scheduleRepository = scheduleRepository;
        this.studioRepository = studioRepository;
        this.userRepository = userRepository;
    }

    public void AddTransaction(TransactionRequest transactionRequest){
        TransactionHistory transactionHistory = new TransactionHistory();
        Optional <Users> user = userRepository.findById(transactionRequest.getUserId());
        Optional<Film> film = filmRepository.findById(transactionRequest.getFilmId());
        Optional<Schedule> schedule = scheduleRepository.findById(transactionRequest.getScheduleId());
        Optional<Studio> studio = studioRepository.findById(transactionRequest.getStudioId());

        transactionHistory.setUsers(user.get());
        transactionHistory.setFilm(film.get());
        transactionHistory.setSchedule(schedule.get());
        transactionHistory.setStudio(studio.get());
        transactionHistory.setStatus(transactionRequest.getStatus());

        transactionHistoryRepository.save(transactionHistory);
    }

    public void UpdateStatusTransaction(String Id, String status){
        Optional<TransactionHistory> transactionHistory = transactionHistoryRepository.findById(Id);
        transactionHistory.get().setStatus(TransactionStatus.valueOf(status));
        transactionHistoryRepository.save(transactionHistory.get());
    }

    public List<TransactionHistoryResponse> getTransactions(){
        return transactionHistoryRepository.getAllTransactions();
    }
}
