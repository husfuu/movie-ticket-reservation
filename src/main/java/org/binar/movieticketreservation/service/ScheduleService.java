package org.binar.movieticketreservation.service;

import org.binar.movieticketreservation.dto.FilmScheduleRequest;
import org.binar.movieticketreservation.dto.FilmScheduleUpdateRequest;
import org.binar.movieticketreservation.dto.ScheduleResponse;
import org.binar.movieticketreservation.entity.Film;
import org.binar.movieticketreservation.entity.Schedule;
import org.binar.movieticketreservation.entity.Studio;
import org.binar.movieticketreservation.exception.IdNotFoundException;
import org.binar.movieticketreservation.repository.FilmRepository;
import org.binar.movieticketreservation.repository.ScheduleRepository;
import org.binar.movieticketreservation.repository.StudioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service @Transactional
public class ScheduleService {
    private final ScheduleRepository scheduleRepository;
    private final FilmRepository filmRepository;
    private final StudioRepository studioRepository;
    @Autowired
    public ScheduleService(ScheduleRepository scheduleRepository, FilmRepository filmRepository, StudioRepository studioRepository){
        this.scheduleRepository = scheduleRepository;
        this.filmRepository = filmRepository;
        this.studioRepository = studioRepository;
    }

    public List<ScheduleResponse> getFilmSchedules(){
        return scheduleRepository.getAllScheduleInformation();
    }

    public void AddFilmSchedules(FilmScheduleRequest filmScheduleRequest){
        Film film = new Film();
        film.setName(filmScheduleRequest.getFilmName());
        film.setIsOnShow(filmScheduleRequest.isOnShow());
        filmRepository.save(film);

        Schedule schedule = new Schedule();
        schedule.setFilm(film);
        schedule.setShow_time(filmScheduleRequest.getShow_time());
        schedule.setStart_time(filmScheduleRequest.getStart_time());
        schedule.setEnd_time(filmScheduleRequest.getEnd_time());
        scheduleRepository.save(schedule);
    }

    public void UpdateFilmSchedule(String id, FilmScheduleUpdateRequest filmScheduleUpdateRequest) throws IdNotFoundException {
        Optional<Schedule> scheduleOptional = scheduleRepository.findById(id);

        if (scheduleOptional.isPresent()){
            throw new IdNotFoundException("schedule with " + id + " is not found!");
        }
        System.out.println("ini errorr!");

        String filmId = scheduleOptional.get().getFilm().getId();
        String studioId = scheduleOptional.get().getStudio().getId();

        Optional<Film> filmOptional = filmRepository.findById(filmId);
        if (filmOptional.isPresent()){
            throw new IdNotFoundException("film with " + id + " is not found!");
        }

        Optional<Studio> studioOptional = studioRepository.findById(studioId);
        if (studioOptional.isPresent()){
            throw new IdNotFoundException("studio with " + id + " is not found!");
        }

        filmOptional.get().setName(filmScheduleUpdateRequest.getFilmName());
        studioOptional.get().setName(filmScheduleUpdateRequest.getStudioName());

        filmRepository.save(filmOptional.get());
        studioRepository.save(studioOptional.get());
    }
}
