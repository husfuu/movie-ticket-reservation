package org.binar.movieticketreservation.entity;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.binar.movieticketreservation.dto.ScheduleResponse;

import javax.persistence.*;
import java.time.LocalDateTime;

@SqlResultSetMapping(
        name = "scheduleMapping",
        classes = {
                @ConstructorResult(
                        targetClass = ScheduleResponse.class,
                        columns = {
                                @ColumnResult(name = "film_name", type = String.class),
                                @ColumnResult(name = "show_time", type = LocalDateTime.class),
                                @ColumnResult(name = "start_time", type = LocalDateTime.class),
                                @ColumnResult(name = "end_time", type = LocalDateTime.class),
                                @ColumnResult(name = "studio_name", type = String.class)
                        }
                )
        }
)
@NamedNativeQuery(
        name = "Schedule.getAllScheduleInformation",
        query = "select\n" +
                "f.name as film_name,\n" +
                "show_time,\n" +
                "start_time,\n" +
                "end_time,\n" +
                "st.name as studio_name\n" +
                "from schedule sc\n" +
                "inner join film f on sc.film_id = f.id\n" +
                "inner join studio st on sc.studio_id = st.id\n" +
                "where f.is_on_show = true",
        resultSetMapping = "scheduleMapping",
        resultClass = ScheduleResponse.class
)

@Entity @Data
@NoArgsConstructor
@AllArgsConstructor
public class Schedule extends BaseEntity{
    @NotNull
    @ManyToOne
    @JoinColumn(name = "film_id")
    private Film film;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "studio_id")
    private Studio studio;

    @NotNull
    private LocalDateTime show_time;

    @NotNull
    private LocalDateTime start_time;

    @NotNull
    private LocalDateTime end_time;
}
