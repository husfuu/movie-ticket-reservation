package org.binar.movieticketreservation.entity;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.validation.constraints.Size;

@Entity @Data
@NoArgsConstructor
@AllArgsConstructor
@SQLDelete(sql = "UPDATE users SET status_record = 'INACTIVE' WHERE id =?")
@Where(clause = "status_record='ACTIVE'")
public class Users extends BaseEntity {
    @NotNull @Size(min = 3)
    private String username;

    @NotNull
    private String email;

    @NotNull @Size(min = 8)
    private String password;
}
