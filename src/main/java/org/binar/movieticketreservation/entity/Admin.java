package org.binar.movieticketreservation.entity;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.persistence.Entity;
import javax.validation.constraints.Size;

@Entity @Data
public class Admin extends BaseEntity {
    @NotNull @Size(min = 3)
    private String username;

    @NotNull @Size(min = 8)
    private String password;
}
