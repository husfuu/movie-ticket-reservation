package org.binar.movieticketreservation.entity;

import lombok.Data;
import org.binar.movieticketreservation.dto.TransactionHistoryResponse;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@SqlResultSetMapping(
        name = "transactionMapping",
        classes = {
                @ConstructorResult(
                        targetClass = TransactionHistoryResponse.class,
                        columns = {
                                @ColumnResult(name = "film_name", type = String.class),
                                @ColumnResult(name = "studio_name", type = String.class),
                                @ColumnResult(name = "show_time", type = LocalDateTime.class),
                                @ColumnResult(name = "start_time", type = LocalDateTime.class),
                                @ColumnResult(name = "end_time", type = LocalDateTime.class),
                                @ColumnResult(name = "username", type = String.class)
                        }
                )
        }
)

@NamedNativeQuery(
        name = "TransactionHistory.getAllTransactions",
        query = "select " +
                "f.name as film_name, " +
                "st.name as studio_name, " +
                "sc.show_time, " +
                "sc.start_time, " +
                "sc.end_time, " +
                "u.username " +
                "from transaction_history th " +
                "inner join film f on th.film_id = f.id " +
                "inner join studio st on studio_id = st.id " +
                "inner join schedule sc on th.schedule_id = sc.id " +
                "inner join users u on th.user_id = u.id;",
        resultSetMapping = "transactionMapping",
        resultClass = TransactionHistoryResponse.class
)


@Entity @Data
public class TransactionHistory extends BaseEntity {
    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id")
    private Users users;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "film_id")
    private Film film;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "schedule_id")
    private Schedule schedule;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "studio_id")
    private Studio studio;

    @NotNull
    @Enumerated(EnumType.STRING)
    private TransactionStatus status= TransactionStatus.PENDING;
}
