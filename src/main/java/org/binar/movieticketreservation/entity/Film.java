package org.binar.movieticketreservation.entity;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Entity @Data
@NoArgsConstructor
@AllArgsConstructor
public class Film extends BaseEntity{
    @NotNull
    private String name;

    @NotNull
    private Boolean isOnShow;
}
