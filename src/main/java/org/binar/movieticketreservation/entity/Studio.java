package org.binar.movieticketreservation.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Entity @Data
@NoArgsConstructor
@AllArgsConstructor
public class Studio extends BaseEntity{
    private String name;
}
