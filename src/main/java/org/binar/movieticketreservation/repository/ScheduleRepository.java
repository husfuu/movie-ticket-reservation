package org.binar.movieticketreservation.repository;

import org.binar.movieticketreservation.dto.ScheduleResponse;
import org.binar.movieticketreservation.entity.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, String> {
    @Query(nativeQuery = true)
    public List<ScheduleResponse> getAllScheduleInformation();

//    @Override
//    <S extends Schedule, R> R findBy(Example<S> example, Function<FluentQuery.FetchableFluentQuery<S>, R> queryFunction);
}
