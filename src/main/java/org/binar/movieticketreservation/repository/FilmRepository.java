package org.binar.movieticketreservation.repository;

import org.binar.movieticketreservation.entity.Film;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FilmRepository extends JpaRepository<Film, String> {
}
