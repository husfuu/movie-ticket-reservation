package org.binar.movieticketreservation.repository;

import org.binar.movieticketreservation.dto.TransactionHistoryResponse;
import org.binar.movieticketreservation.entity.TransactionHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TransactionHistoryRepository extends JpaRepository<TransactionHistory, String>{
    @Query(nativeQuery = true)
    List<TransactionHistoryResponse> getAllTransactions();
}
