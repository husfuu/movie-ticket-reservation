package org.binar.movieticketreservation.repository;

import org.binar.movieticketreservation.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<Users, String> {
    @Query("SELECT u FROM Users u WHERE u.username = ?1")
    Optional<Users> findUserByUsername(String username);
}

//public interface UserRepository extends PagingAndSortingRepository<Users, String> {
////    @Query("SELECT u FROM users u WHERE u.username = ?1")
////    Optional<Users> findUserByUsername(String username);
//}
