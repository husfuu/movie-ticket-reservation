package org.binar.movieticketreservation.exception;

public class UsernameTakenException extends Exception{
    public UsernameTakenException(){}
    
    public UsernameTakenException(String message){
        super(message);
    }
}
