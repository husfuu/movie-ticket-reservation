package org.binar.movieticketreservation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FilmScheduleRequest {
    // for film table
    private String filmName;
    private boolean isOnShow;
    // for schedule table
    private LocalDateTime show_time;
    private LocalDateTime start_time;
    private LocalDateTime end_time;
}
