package org.binar.movieticketreservation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.binar.movieticketreservation.entity.TransactionStatus;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TransactionRequest {
    @Enumerated(EnumType.STRING)
    private TransactionStatus status= TransactionStatus.PENDING;
    private String filmId;
    private String scheduleId;
    private String studioId;
    private String userId;
}
