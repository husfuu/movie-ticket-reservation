package org.binar.movieticketreservation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ScheduleResponse {
    private String filmName;
    private LocalDateTime showTime;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String studioName;
}
