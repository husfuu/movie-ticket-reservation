package org.binar.movieticketreservation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TransactionHistoryResponse {
    private String filmName;
    private String studioName;
    private LocalDateTime showTime;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String username;
}
