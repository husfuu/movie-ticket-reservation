create table admin (
    id varchar(36),
    primary key (id)
);

create table users (
    id varchar(36),
    primary key (   id)
);

create table film (
    id varchar(36),
    primary key (id)
);

create table schedule (
    id varchar(36),
    primary key (id)
);

create table transaction_history (
    id varchar(36),
    primary key (id)
);